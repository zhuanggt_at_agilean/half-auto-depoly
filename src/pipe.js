const targets = require('./target');

const pipe = ({ env, config, nodes, services, processes }) => {
    targets.forEach(target => {
        target({env, config, nodes, services, processes});
    });
};

module.exports = pipe;