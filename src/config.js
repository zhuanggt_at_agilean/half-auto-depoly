const fs = require('fs');
const path = require('path');

const configFilePath = path.resolve(__dirname, process.env.CONFIG_FILE);
console.log('configFilePath', configFilePath);

const fileContent = fs.readFileSync(configFilePath, { encoding: 'utf-8' });

console.log('fileContent', fileContent);

const fileObject = JSON.parse(fileContent);

const config = {
    access: {
        username: fileObject.access.username,
        password: fileObject.access.password,
        host: fileObject.access.host,
        entry: fileObject.access.entry
    },
    accessPage: {
        orgId: fileObject.accessPage.orgId,
        processMenuId: fileObject.accessPage.processMenuId,
        machineMenuId: fileObject.accessPage.machineMenuId,
        serviceMenuId: fileObject.accessPage.serviceMenuId,
    },
    properties: {
        plugin: {
            jarFileName: "b12a0a065e3849bc88e66f2c2fac6ccc",
        },
        env: {
            user: fileObject.properties.env.user,
            appDir: fileObject.properties.env.appDir,
            nasPath: fileObject.properties.env.nasPath,
            appConfigDir: fileObject.properties.env.appConfigDir,
            appolo: fileObject.properties.env.appolo,
            linkToMachines: '2b679179954c456f8951a76876d0823d',
            linkToProcesses: '20166a522e2b4633a10eb154f85d0d1b'
        },
        service: {
            port: fileObject.properties.service.port,
            jarFileName: fileObject.properties.service.jarFileName,
            type: fileObject.properties.service.type,
            namespaces: fileObject.properties.service.namespace,
        },
        process: {
            env: fileObject.properties.process.env,
            serviceIndex: fileObject.properties.process.serviceIndex,
            toService: fileObject.properties.process.toService,
            serviceDuty: fileObject.properties.process.serviceDuty,
            toMachine: fileObject.properties.process.toMachine,
            namespaces: fileObject.properties.process.namespace,
            memoryUnit: fileObject.properties.process.memoryUnit,
            memory: fileObject.properties.process.memory,
            baseInstall: fileObject.properties.process.baseInstall,
            baseData: fileObject.properties.process.baseData,
            toPlugin: "5878cc2a270148029403913c1becdd94"
        }
    },
    env: {
        dist: fileObject.env.dist,
        template: fileObject.env.template,
    }
};

module.exports = config;