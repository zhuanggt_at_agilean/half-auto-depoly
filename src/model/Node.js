class Node {
    constructor([machine, processes]) {
        this.machine = machine;
        this.alias = processes.map(({ service, serviceIndex, serviceDuty }) =>
            [service.name, serviceIndex, serviceDuty].filter(i => i !== undefined && i !== null).join('-')).join('_');
    }
};

module.exports = Node;