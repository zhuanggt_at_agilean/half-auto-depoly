class Env {
    constructor({ redisPass, user, envFlag, nasPath, appolo, appDir, appConfigDir, machines, processes }) {
        this.user = user;
        this.envFlag = envFlag;
        this.nasPath = nasPath;
        this.hollowPath = nasPath + "/hollow";
        this.ossPath = nasPath + "/oss";
        this.installPath = {
            script: nasPath + "/kb/install/script",
            jar: nasPath + "/kb/install/jar",
        }
        this.appDir = appDir;
        this.appConfigDir = appConfigDir;
        this.appolo = appolo;
        this.machines = machines;
        this.processes = processes;
        this.redisPass = this.redisPass;
    }
};

module.exports = Env;