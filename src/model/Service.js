class Service {
    constructor({ id, name, type, namespaces, jarFileName, port, hosts }) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.namespaces = namespaces;
        this.jarFileName = jarFileName;
        this.port = port;
        this.hosts = hosts || [];
    }

    initNamespace(namespacesLookup) {
        this.namespaces = (this.namespaces || []).map(id => (namespacesLookup[id] || {}).name).filter(str => !!str);
    }

    initHosts(serviceToProcess) {
        this.hosts = (serviceToProcess[this.id] || []).map(process => process.machine);
    }

};

module.exports = Service;