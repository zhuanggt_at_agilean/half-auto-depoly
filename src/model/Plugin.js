class Plugin {
    constructor({ id, name, jarFileName }) {
        this.id = id;
        this.name = name;
        this.jarFileName = jarFileName;
    }
};

module.exports = Plugin;