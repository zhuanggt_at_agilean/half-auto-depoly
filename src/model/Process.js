class Process {
    constructor({
        name,
        machine,
        memory,
        baseInstall,
        baseData,
        memoryUnit,
        serviceIndex,
        serviceDuty,
        service,
        namespaces,
        plugins
    }) {
        this.name = name;
        this.serviceIndex = serviceIndex;
        this.serviceDuty = serviceDuty;
        this.baseInstall = baseInstall;
        this.baseData = baseData;
        this.machine = machine;
        this.memory = memory;
        this.memoryUnit = memoryUnit;
        this.service = service;
        this.namespaces = namespaces;
        this.plugins = plugins;
    }

    initMachine(machine) {
        this.machine = machine;
    }

    initService({ service }) {
        this.service = service;
        this.nodeAlias = [service.name, this.serviceIndex, this.serviceDuty].filter(i => i !== undefined && i !== null).join('-');
        this.nodeAlias2 = [service.name, this.serviceDuty].filter(i => i !== undefined && i !== null).join('-');
        this.namespaces = [...this.namespaces, ...this.service.namespaces];
    }

    initNamespace(namespacesLookup) {
        this.namespaces = (this.namespaces || []).map(id => (namespacesLookup[id] || {}).name).filter(str => !!str);
    }

    initPlugins(pluginsLookup) {
        this.plugins = (this.pluginsLookup || []).map(id => (pluginsLookup[id] || {}).name).filter(str => !!str);
    }

};

module.exports = Process;