const SERVICE_NAME = {
    CARD: 'card',
    SCHEMA: 'schema',
    RBAC: 'rbac',
    VIEW: 'view',
    MAIN: 'main',
    NSTATS: 'nstats',
    HISTORY: 'history',
    MULTI: 'multi',
    FILTER: 'filter',
    NOTI: 'notification',
    OSS: 'oss',
    API: 'api',
    SYNC: 'sync',
    UI: 'ui',
    ZK: 'zk',
    KAFKA: 'kafka',
    DEVOPS: 'devops',
    DRUID: 'druid'
};

const DUTY = {
    WS: {
        name: 'ws',
        service: SERVICE_NAME.MAIN
    },
    BOT: {
        name: 'bot',
        service: SERVICE_NAME.MULTI
    },
    BIZRULE: {
        name: 'bizrule',
        service: SERVICE_NAME.MULTI
    },
    COMMENT: {
        name: 'comment',
        service: SERVICE_NAME.HISTORY
    },
};

module.exports = {
    SERVICE_NAME,
    DUTY
}