class Machine {
    constructor({ id, ip }) {
        this.id = id;
        this.ip = ip;
    }
};

module.exports = Machine;
