const fs = require('fs');
const path = require('path');

const DIR = 'copyJar';

// ossutil cp -f $remotePath/card.jar $jarPath/
module.exports = ({ env, nodes, config, services, processes }) => {
    const jarFileNames = services.filter(s => s.type === '应用服务').map(s => s.jarFileName);
    const tpl = jarFileName => `ossutil cp -f $remotePath/${jarFileName} $dest`;
    const tplFileContent = fs.readFileSync(path.join(__dirname, 'copy.sh.tpl')).toString();
    const content = tplFileContent.split('%copyStatement%').join(jarFileNames.map(jarFileName => tpl(jarFileName)).join("\n"));
    const destDir = path.join(config.env.dist, DIR);
    if (!fs.existsSync(destDir)) {
        fs.mkdirSync(destDir);
    }
    fs.writeFileSync(
        path.join(config.env.dist, DIR, 'copy.sh'),
        content
    );
}