const fs = require('fs');
const path = require('path');

const paramCheck = () => {

};

const toRunStatement = process => {
    return [
        `echo "running $1 on node: ${process.nodeAlias}[${process.machine}]";`,
        `ssh ${process.nodeAlias} 'bash -s' < $1`,
        `echo "done $1 on node: ${process.nodeAlias}[${process.machine}]"`,
        'echo " "',
        'echo "___"'
    ].join("\n");
};

module.exports = ({ config, nodes, services, processes }) => {
    const exists = {};
    for (let process of processes) {
        if (exists[process['machine']]) {
            continue;
        }
        exists[process['machine']] = process;
    }
    const content = [
        paramCheck(),
        Object.values(exists).map(toRunStatement).join("\n")
    ].join("\n");
    fs.writeFileSync(
        path.join(config.env.dist, 'remoteCommand'),
        content
    );
};