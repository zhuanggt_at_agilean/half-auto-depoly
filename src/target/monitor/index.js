const fs = require('fs');
const path = require('path');

const header = () => fs.readFileSync(path.join(__dirname, 'template.yml')).toString('utf-8');

const toConfig = node => {
    return [
        `    - targets: ['${node.machine}:9100']`,
        `      labels:`,
        `        instance: ${node.alias}`,
    ].join("\n");
};

module.exports = ({ nodes, config, services, processes }) => {
    const content = [
        header(),
        nodes.sort().map(toConfig).join("\n")
    ].join("\n");
    fs.writeFileSync(
        path.join(config.env.dist, 'prometheusConfig.yml'),
        content
    );
};