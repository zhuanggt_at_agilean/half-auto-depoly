module.exports = [
    require('./sshConfig'),
    require('./remoteCommand'),
    require('./monitor'),
    require('./kafkaZkConfig'),
    require('./config'),
    require('./appRunScript'),
    require('./copyJar')
];