#!/bin/bash
cd %appDir%
jarFileName=%jarFileName%
configDir=%configDir%
ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}' | xargs -i kill -9 {}
nohup java -Xmx%memory% -jar ${jarFileName} --spring.config.location=%config% > /dev/null 2>&1 &
