const fs = require('fs');
const path = require('path');
const {
    prop,
    propEq,
    map,
    join,
    compose,
    concat
} = require('ramda');

const DIR = 'run';
const DEPLOY = 'deploy';

module.exports = ({ env, nodes, config, services, processes }) => {
    const appProcesses = processes.filter(compose(propEq('type', '应用服务'), prop('service')));
    handleRunScript(env, config, appProcesses);
    handleStopScript(env, config, appProcesses);
    handleDepolyScript(env, config, appProcesses);
};

function handleDepolyScript(env, config, appProcesses) {
    handleInstallScript(env, config, appProcesses);
    handleDeployScript(env, config, appProcesses);
};

function runScriptName(alias) {
    return `${alias}-run.sh`
};

function stopScriptName(alias) {
    return `${alias}-stop.sh`
};

function propertiesFileName(alias) {
    return `${alias}.properties`
}

function sharePropertiesFileName(alias) {
    return `share.yml`
}

function handleInstallScript(env, config, appProcesses) {
    const destPath = path.join(config.env.dist, DEPLOY);
    if (!fs.existsSync(destPath)) {
        fs.mkdirSync(destPath);
    }
    const tpl = fs.readFileSync(path.join(__dirname, 'copyScript.tpl')).toString('utf8');
    const scriptPath = env.installPath.script;
    const appDir = env.appDir;
    let toWrite = [];
    for (let appProcess of appProcesses) {
        let content = tpl;
        content = content.split('%node%').join(appProcess.nodeAlias);
        content = content.split('%srcOfStart%').join(scriptPath + "/" + runScriptName(appProcess.nodeAlias2));
        content = content.split('%srcOfStop%').join(scriptPath + "/" + stopScriptName(appProcess.nodeAlias2));
        content = content.split('%srcOfProperties%').join(scriptPath + "/" + propertiesFileName(appProcess.nodeAlias2));
        content = content.split('%srcOfShare%').join(scriptPath + "/" + sharePropertiesFileName(appProcess.nodeAlias2));
        content = content.split('%dest%').join(appDir);
        toWrite.push(content);
    }
    const destFile = path.join(destPath, "install.sh");
    fs.writeFileSync(destFile, toWrite.join('\n'));
};

function handleDeployScript(env, config, appProcesses) {
    const destPath = path.join(config.env.dist, DEPLOY);
    if (!fs.existsSync(destPath)) {
        fs.mkdirSync(destPath);
    }
    const tpl = fs.readFileSync(path.join(__dirname, 'copyJar.tpl')).toString('utf8');
    const jarPath = env.installPath.jar;
    const appDir = env.appDir;
    let toWrite = [];
    for (let appProcess of appProcesses) {
        let content = tpl;
        content = content.replace('%node%', appProcess.nodeAlias);
        content = content.replace('%srcOfJar%', jarPath + "/" + appProcess.service.jarFileName);
        content = content.replace('%dest%', appDir);
        toWrite.push(content);
    }
    const destFile = path.join(destPath, "updateLatest.sh");
    fs.writeFileSync(destFile, toWrite.join('\n'));
}

function handleRunScript(env, config, appProcesses) {
    const tpl = fs.readFileSync(path.join(__dirname, 'run.tpl')).toString('utf8');
    const render = ({ memory, memoryUnit, nodeAlias2, service, namespaces }) => {
        let ret = tpl;
        ret = ret.replace('%appDir%', env.appDir);
        ret = ret.replace('%configDir%', env.appConfigDir);
        ret = ret.replace('%jarFileName%', service.jarFileName);
        ret = ret.replace('%memory%', memory + memoryUnit);
        if (env.appolo) {

        } else {
            const propertieFiles = ['app', 'share'];
            if (namespaces.includes('cardhollow')) {
                propertieFiles.push('cardhollow');
            }
            ret = ret.replace(
                '%config%',
                compose(
                    join(','),
                    map(propertieFile => {
                        if (propertieFile.includes("share")) {
                            return `$\{configDir\}/share.yml`;
                        } else {
                            return `$\{configDir\}/${propertieFile}.properties`
                        }
                    }),
                    map(propertieFile => {
                        if (propertieFile === 'app') {
                            return service.name;
                        } else {
                            return propertieFile;
                        }
                    })
                )(propertieFiles)
            );
        }
        return {
            content: ret,
            fileName: runScriptName(nodeAlias2)
        };
    };
    const destPath = path.join(config.env.dist, DIR);
    if (!fs.existsSync(destPath)) {
        fs.mkdirSync(destPath);
    }
    for (let appProcess of appProcesses) {
        const { content, fileName } = render(appProcess);
        const destFile = path.join(destPath, fileName);
        fs.writeFileSync(destFile, content);
    }
};

function handleStopScript(env, config, appProcesses) {
    const tpl = fs.readFileSync(path.join(__dirname, 'stop.tpl')).toString('utf8');
    const render = ({ nodeAlias2, service }) => {
        let ret = tpl;
        ret = ret.replace('%appDir%', env.appDir);
        ret = ret.replace('%jarFileName%', service.jarFileName);
        return {
            content: ret,
            fileName: stopScriptName(nodeAlias2)
        }
    }
    const destPath = path.join(config.env.dist, DIR);
    if (!fs.existsSync(destPath)) {
        fs.mkdirSync(destPath);
    }
    for (let appProcess of appProcesses) {
        const { content, fileName } = render(appProcess);
        const destFile = path.join(destPath, fileName);
        fs.writeFileSync(destFile, content);
    }
};