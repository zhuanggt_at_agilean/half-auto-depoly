#!/bin/bash
cd %appDir%
jarFileName=%jarFileName%
ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}' | xargs kill -9 {}