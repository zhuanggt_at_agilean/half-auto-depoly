const fs = require('fs');
const path = require('path');
const {
    map,
    join,
    prop,
    compose,
    startsWith
} = require('ramda');

function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
};

const render = (tpl, replaces) => processInfo => {
    let ret = tpl;
    for (let replace of replaces) {
        if (isFunction(replace.value)) {
            ret = ret.split(replace.placeHolder).join(replace.value(processInfo));
        } else {
            ret = ret.split(replace.placeHolder).join(replace.value);
        }
    }
    return ret;
};

const handleKafkaCluster = ({ config, kafkaProcesses, zkProcesses }) => {
    const kafkaPropertiesTpl = fs.readFileSync(path.join(__dirname, 'cluster.server.properties')).toString('utf8');
    const renderer = render(
        kafkaPropertiesTpl,
        [
            {
                value: prop('serviceIndex'),
                placeHolder: "%brokerId%"
            },
            {
                value: prop('baseData'),
                placeHolder: "%logDirs%"
            },
            {
                value: prop('machine'),
                placeHolder: '%host%'
            },
            {
                value: kafkaProcesses.length,
                placeHolder: '%numberOfService%'
            },
            {
                value: compose(join(','), map(prop('machine')))(zkProcesses),
                placeHolder: '%zkService%'
            },
        ]
    );
    for (let kafkaProcess of kafkaProcesses) {
        const configFileContent = renderer(kafkaProcess);
        fs.writeFileSync(
            path.join(config.env.dist, kafkaProcess.nodeAlias + '.server.properties'),
            configFileContent
        );
    }
};

const handleZkCluster = ({ config, kafkaProcesses, zkProcesses }) => {
    const zkCfgFileTpl = fs.readFileSync(path.join(__dirname, 'cluster.zoo_sample.cfg')).toString('utf8');
    const toDesc = ({ serviceIndex, machine }) => `server.${serviceIndex}=${machine}:2888:3888`;
    const serverDesc = join('\n', map(toDesc)(zkProcesses));
    const renderer = render(
        zkCfgFileTpl,
        [
            {
                value: item => prop('baseInstall')(item) + "/data",
                placeHolder: '%dataDir%'
            },
            {
                value: item => prop('baseInstall')(item) + "/logs",
                placeHolder: '%dataLogDir%'
            },
            {
                value: serverDesc,
                placeHolder: '%serverDesc%'
            }
        ]
    );
    for (let zkProcess of zkProcesses) {
        const configFileContent = renderer(zkProcess);
        fs.writeFileSync(
            path.join(config.env.dist, zkProcess.nodeAlias + '.zoo.cfg'),
            configFileContent
        );
    }
    const toCreateMyIdStat = ({ nodeAlias, serviceIndex, baseInstall }) => {
        return [
            `ssh ${nodeAlias} 'mkdir -p ${baseInstall}/data';`,
            `ssh ${nodeAlias} 'mkdir -p ${baseInstall}/logs';`,
            `ssh ${nodeAlias} 'echo ${serviceIndex} > ${baseInstall}/data/myid';`
        ].join('\n');
    };
    compose(
        content => fs.writeFileSync(
            path.join(config.env.dist, 'createMyId.sh'),
            content
        ),
        join('\n'),
        map(toCreateMyIdStat)
    )(zkProcesses);
};

module.exports = ({ nodes, config, services, processes }) => {
    const kafkaProcesses = processes.filter(compose(startsWith('kafka'), prop('name')));
    const zkProcesses = processes.filter(compose(startsWith('zk'), prop('name')));
    if (kafkaProcesses.length > 1) {
        handleKafkaCluster({ config, kafkaProcesses, zkProcesses });
        handleZkCluster({ config, kafkaProcesses, zkProcesses });
    } else {
        // TODO: 处理单机版本
    }
};