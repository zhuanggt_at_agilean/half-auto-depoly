const fs = require('fs');
const path = require('path');
const { compose, replace, map } = require('ramda');

const ROLE = {
    MASTER: 'master',
    SLAVE: 'slave',
};

const handleTemplate = kvs => compose(...map(({ k, v }) => replace(k, v), kvs));

const getConfig = ({ processes, env }) => {
    const configs = [];
    const redisNodes = processes.filter(p => p.nodeAlias.includes("redis"));
    for (let index of ["1", "2", "3"]) {
        for (let p of redisNodes) {
            if (p.nodeAlias.includes(index)) {
                const portMaster = 7000 + parseInt(index);
                configs.push({
                    host: p.machine,
                    port: portMaster,
                    role: ROLE.MASTER,
                    baseInstall: p.baseInstall,
                    baseData: p.baseData,
                    redisPass: env.redisPass,
                    redisConf: path.join(p.baseData, "" + portMaster, "redis.conf"),
                });
                const portSlave = 8000 + parseInt(index);
                configs.push({
                    host: p.machine,
                    port: portSlave,
                    role: ROLE.SLAVE,
                    baseInstall: p.baseInstall,
                    baseData: p.baseData,
                    redisPass: env.redisPass,
                    redisConf: path.join(p.baseData, "" + portSlave, "redis.conf"),
                });
            }
        }
    };
    return configs;
};

const toKvsForConfg = config => [
    {
        k: /%bind%/g,
        v: config.host
    },
    {
        k: /%port%/g,
        v: config.port
    },
    {
        k: /%dir%/g,
        v: path.join(config.baseData, "" + config.port)
    },
    {
        k: /%pidfile%/g,
        v: path.join(config.baseData, "" + config.port, 'pid')
    },
    {
        k: /%logfile%/g,
        v: path.join(config.baseData, "" + config.port, 'log')
    },
    {
        k: /%pass%/g,
        v: config.redisPass
    },
];

const toKvsForStartSh = config => [
    {
        k: /%redis_server_bin%/g,
        v: path.join(config.baseInstall, 'src/redis-server')
    },
    {
        k: /%confg%/g,
        v: config.redisConf
    },
];

module.exports = ({ nodes, config, services, processes, env }) => {
    // 默认为三节点，默认端口为 7001,7002,7003，目前不需要更灵活的配置
    const { dist } = config.env;
    const output = path.join(dist, 'redis-cluster');
    if (!fs.existsSync(output)) {
        fs.mkdirSync(output);
    }
    const confFileTemplate = fs.readFileSync(path.join(__dirname, "template.conf")).toString('utf8');
    const startShTemplate = fs.readFileSync(path.join(__dirname, "start.sh.template")).toString('utf8');
    const configs = getConfig({ processes, env });
    configs.forEach(config => {
        const processDir = path.join(output, "" + config.port);
        if (!fs.existsSync(processDir)) {
            fs.mkdirSync(processDir);
        }
        let confFileContent = handleTemplate(toKvsForConfg(config))(confFileTemplate);
        fs.writeFileSync(
            path.join(processDir, "redis.conf"),
            confFileContent
        );
        let startShContent = handleTemplate(toKvsForStartSh(config))(startShTemplate);
        fs.writeFileSync(
            path.join(processDir, "start.sh"),
            startShContent
        );
    });
};