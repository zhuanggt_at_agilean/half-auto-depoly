const index = require('./index');

index({
    config: {
        env: {
            dist: '/Users/zhuangguangtuan/bin/many-redis'
        }
    },
    env: {
        redisPass: 'kanban'
    },
    processes: [
        {
            nodeAlias: 'redis-1',
            machine: '127.0.0.1',
            baseInstall: '/Users/zhuangguangtuan/bin/many-redis/redis-5.0.5',
            baseData: '/Users/zhuangguangtuan/bin/many-redis/redis-cluster'
        },
        {
            nodeAlias: 'redis-2',
            machine: '127.0.0.1',
            baseInstall: '/Users/zhuangguangtuan/bin/many-redis/redis-5.0.5',
            baseData: '/Users/zhuangguangtuan/bin/many-redis/redis-cluster'
        },
        {
            nodeAlias: 'redis-3',
            machine: '127.0.0.1',
            baseInstall: '/Users/zhuangguangtuan/bin/many-redis/redis-5.0.5',
            baseData: '/Users/zhuangguangtuan/bin/many-redis/redis-cluster'
        },
    ]
});