const fs = require('fs');
const path = require('path');

module.exports = ({ env, config, nodes, services, processes }) => {
    const content = [
        'Host *',
        `  User ${env.user}`,
        '  Port 22',
        processes.map(process => `Host ${process.nodeAlias}\n  Hostname ${process.machine}`).join("\n")
    ].join("\n");
    fs.writeFileSync(
        path.join(config.env.dist, 'sshconfig'),
        content
    );
};