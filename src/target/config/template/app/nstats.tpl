server.port=8894
spring.redis.database=13
spring.kafka.consumer.properties.spring.json.trusted.packages=cn.agilean.valuekanban.event
app.cache.maximum.size=200000
spring.kafka.consumer.properties.spring.json.value.default.type=cn.agilean.valuekanban.event.KafkaContext
spring.kafka.consumer.value-deserializer=org.springframework.kafka.support.serializer.JsonDeserializer
spring.kafka.consumer.group-id=statsGroup
spring.application.name=service-nstats