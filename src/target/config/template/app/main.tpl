app.service.nodes.nstats=%nstatsHost%
app.service.nodes.filter=%filterHost%
app.service.nodes.oss=%ossHost%
app.service.nodes.schema=%schemaHost%
app.service.nodes.view=%viewHost%
app.service.nodes.multi=%multiHost%
app.service.nodes.history=%historyHost%
app.service.nodes.notice=%notificationHost%
app.service.nodes.api=%apiHost%
app.service.nodes.devops=%devopsHost%
app.service.nodes.card=%cardHost%
app.service.nodes.kanban=%mainHost%
app.service.nodes.rbac=%rbacHost%
app.service.nodes.nsync=%syncHost%

spring.redis.database=5
spring.application.name=service-main
spring.mvc.async.request-timeout=300000

app.preview.enabled=true
app.preview.url=http\://%ossHost%/api/v1/preview/mark

server.port=8880