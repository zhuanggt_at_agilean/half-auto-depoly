spring:
    mail:
        default-encoding: UTF-8
        host: %mailHost%
        jndi-name: 'null'
        password: %mailPassword%
        port: 80
        properties:
            mail:
                smtp:
                    auth: true
        protocol: smtp
        receive:
            host: %mailReceiveHost%
            port: 110
            protocol: pop3
        test-connection: false
        username: %mailUser%