spring:
    redis:
        host: %redishost%
        port: 6379
        lettuce:
            pool:
                max-active: 500
                max-idle: 500
                max-wait: -1ms
                min-idle: 10
            shutdown-timeout: 200ms
        password: kanbanAg1
