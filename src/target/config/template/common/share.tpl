agilean:
    bizrule:
        base:
            url: http://%bizrule%/
    bot:
        base:
            url: http://%bot%/
    comment:
        base:
            url: http://%comment%/
    druid:
        base:
            url: http://%druid%/
    filter:
        service:
            list: %filterList%
            port: %filterPort%
    history:
        base:
            url: http://%history%/
    main:
        base:
            url: http://%main%/
    nfilter:
        base:
            url: http://%filter%/
    notice:
        base:
            url: http://%notification%/
    nstats:
        base:
            url: http://%nstats%/
    stats:
        service:
            list: %nstatsList%
            port: %nstatsPort%
    openapi:
        base:
            url: http://%api%/
    oss:
        base:
            url: http://%oss%/
    rbac:
        base:
            url: http://%rbac%/
    schema:
        base:
            url: http://%schema%/
        service:
            list: %schemaList%
            port: %schemaPort%
    token: 8648E7928F01F291189241C73F2B0904
    vu:
        service:
            list: %vuList%
            port: %vuPort%
    valueunit:
        base:
            url: http://%card%/
    view:
        base:
            url: http://%view%/
        service:
            list: %viewList%
            port: %viewPort%
    ws:
        url: http://%ws%/ws
app:
    assistant: info@agilean.cn
    attachment:
        max-file-size: 900
    auto:
        sendInviteMail: true
        sendMail: true
    customer:
        key: Agilean
    default:
        domain: https://tkb.agilean.cn
        name: Agilean Kanban
    env: %envFlag%
    error:
        password:
            count:
                limit: 5
                expire: 1
                show:
                    code: 3
    mail:
        bcc:
            list: bcc@agilean.cn
        regex: ^[a-zA-Z0-9_-]+([\\.]?)+([a-zA-Z0-9_-]+)+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$
    temp:
        dir: /tmp
    user:
        level: Sandwich
    verify:
        code:
            enabled: true
attachment:
    enabled: true
card:
    hollow:
        cache:
            enabled: false
export:
    task:
        enable: true
        expiration:
            days: 1
font:
    size: 50
hollow:
    cache:
        cycles:
            time:
                comment: 3000
                container: 3000
                rbac: 3000
                schema: 3000
                valueUnit: 200
                valueUnitType: 3000
        path: %hollowCachePath%
http:
    connection:
        timeout: 5000
    error:
        retry:
            times: 0
    keep:
        alive:
            time: 5000
    pool:
        connection:
            max: 600
    request:
        timeout: 5000
    route:
        per:
            max: 300
    socket:
        timeout: 20000
image:
    comment:
        enabled: true
    desc:
        enabled: true
libreoffice:
    file:
        temp:
            world:
                input: /tmp/input
            pdf:
                output: /tmp/output
    pdf:
        trans:
            timeout: 500000
    scrip:
        path: /tmp/input/trans.sh
    tool:
        install: /opt/libreoffice6.2/program
management:
    endpoint:
        health:
            show-details: always
        metrics:
            enabled: true
        prometheus:
            enabled: true
    endpoints:
        web:
            exposure:
                include: '*'
    metrics:
        export:
            prometheus:
                enabled: true
matrix:
    theta: 25
oss:
    base:
        path: %ossPath%
    private:
        suffix:
            permit: sketch,psd,pptx,ppt,docx,doc,xlsx,xls,jpg,jpeg,png,bmp,gif,zip,rar,7z,gz,md,txt,csv,pdf
        type:
            limit: true
    type: AgileanOss
private:
    oss:
        endpoint: %ossAccess%
report:
    event:
        loop:
            tick:
                span:
                    millis: 5000
    export:
        flow-velocity: 0-9:500,9-12:50,12-24:100
        schedule: 0 0/1 * * * ?
server:
    max-http-header-size: 1048576
    servlet:
        session:
            cookie:
                max-age: 777600
            timeout: 12960m
spring:
    cache:
        type: redis
    codec:
        max-in-memory-size: 20MB
    datasource:
        username: kanban
        dbname: kanban
        password: kanbanAg1
        url: jdbc:mysql://%mysqlhost%:3306/kanban?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai
    jpa:
        hibernate:
            ddl-auto: none
        show-sql: false
    kafka:
        bootstrap-servers: %kafka%
