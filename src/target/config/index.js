const {
    compose,
    find,
    propEq,
    join,
    defaultTo,
    map,
    prop,
    head,
    pathEq,
    filter,
    concat,
    replace
} = require('ramda');

const fs = require('fs');
const path = require('path');

const Const = require('../../model/Const');

const DIR = 'config';

function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
};

const buildServiceAccessListGetter = services => serviceName => {
    const service = find(propEq('name', serviceName))(services);
    if (!service) {
        return 'localhost';
    }
    const { hosts, port } = service;
    return compose(
        join(','),
        map(host => `${host}:${port}`),
        defaultTo([])
    )(hosts);
};

const debug = any => {
    console.log(JSON.stringify(any, null, 4));
    return any;
};

const buildServiceHostGetter = services => serviceName => {
    const service = find(propEq('name', serviceName))(services);
    if (!service) {
        return 'localhost';
    }
    const { hosts, port } = service;
    return compose(
        host => `${host}:${port}`,
        defaultTo({}),
        head,
        defaultTo([]),
    )(hosts);
};

const render = (tpl, replaces) => processInfo => {
    let ret = tpl;
    for (let replace of replaces) {
        if (isFunction(replace.value)) {
            ret = ret.split(replace.placeHolder).join(replace.value(processInfo));
        } else {
            ret = ret.split(replace.placeHolder).join(replace.value);
        }
    }
    return ret;
};

module.exports = ({ env, nodes, config, services, processes }) => {
    const appTplDir = path.join(__dirname, 'template/app');
    const commonTplDir = path.join(__dirname, 'template/common');
    const configDirPath = path.join(config.env.dist, DIR);
    if (!fs.existsSync(configDirPath)) {
        fs.mkdirSync(configDirPath);
    }

    handleAppProperties(services, appTplDir, configDirPath);
    handleCommonProperties(env, processes, services, commonTplDir, configDirPath);
};

const replaceTargets = [
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('nstat')).map(prop('machine')).join(',');
        const port = services.filter(s => s.name.includes("nstat")).map(prop('port'));
        return compose(
            replace(/%nstatsList%/g, list),
            replace(/%nstatsPort%/g, port)
        )(input);
    },
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('filter')).map(prop('machine')).join(',');
        const port = services.filter(s => s.name.includes("filter")).map(prop('port'));
        return compose(
            replace(/%filterList%/g, list),
            replace(/%filterPort%/g, port)
        )(input);
    },
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('schema')).map(prop('machine')).join(',');
        const port = services.filter(s => s.name.includes("schema")).map(prop('port'));
        return compose(
            replace(/%schemaList%/g, list),
            replace(/%schemaPort%/g, port)
        )(input);
    },
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('view')).map(prop('machine')).join(',');
        const port = services.filter(s => s.name.includes("view")).map(prop('port'));
        return compose(
            replace(/%viewList%/g, list),
            replace(/%viewPort%/g, port)
        )(input);
    },
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('card') && p.serviceDuty === 'api').map(prop('machine')).join(',');
        const port = services.filter(s => s.name.includes("card")).map(prop('port'));
        return compose(
            replace(/%vuList%/g, list),
            replace(/%vuPort%/g, port)
        )(input);
    },
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('redis')).map(prop('machine')).join(',');
        return compose(
            replace(/%redishost%/g, list)
        )(input);
    },
    ({ processes, services }) => input => {
        const list = processes.filter(p => p.name.includes('mysql')).map(prop('machine')).join(',');
        return compose(
            replace(/%mysqlhost%/g, list)
        )(input);
    },
    ({ env, processes, services }) => input => {
        const { hollowPath, envFlag, ossPath } = env.hollowPath;
        return compose(
            replace(/%hollowCachePath%/g, hollowPath),
            replace(/%envFlag%/g, envFlag),
            replace(/%ossPath%/g, ossPath),
        )(input);
    },
]

function handleCommonProperties(env, processes, services, commonTplDir, configDirPath) {
    const serviceAccess = buildServiceHostGetter(services);
    const commonTpls = fs.readdirSync(commonTplDir);
    for (let commonTpl of commonTpls) {
        const renderer = render(
            fs.readFileSync(path.join(commonTplDir, commonTpl)).toString("utf8"),
            concat(
                [
                    Const.SERVICE_NAME.CARD,
                    Const.SERVICE_NAME.RBAC,
                    Const.SERVICE_NAME.SCHEMA,
                    Const.SERVICE_NAME.VIEW,
                    Const.SERVICE_NAME.FILTER,
                    Const.SERVICE_NAME.MAIN,
                    Const.SERVICE_NAME.NOTI,
                    Const.SERVICE_NAME.HISTORY,
                    Const.SERVICE_NAME.MULTI,
                    Const.SERVICE_NAME.SYNC,
                    Const.SERVICE_NAME.NSTATS,
                    Const.SERVICE_NAME.DEVOPS,
                    Const.SERVICE_NAME.OSS,
                    Const.SERVICE_NAME.API,
                    Const.SERVICE_NAME.DRUID,
                    Const.DUTY.WS,
                    Const.DUTY.BOT,
                    Const.DUTY.BIZRULE,
                    Const.DUTY.COMMENT,
                ].map(service => {
                    if (service.name) {
                        return {
                            value: serviceAccess(service.service),
                            placeHolder: `%${service.name}%`
                        }
                    } else {
                        return {
                            value: serviceAccess(service),
                            placeHolder: `%${service}%`
                        }
                    }
                }),
                [
                    {
                        placeHolder: `%${Const.SERVICE_NAME.KAFKA}%`,
                        value: (processes => {
                            const kafkaProcesses = filter(pathEq(['service', 'name'], Const.SERVICE_NAME.KAFKA))(processes);
                            return compose(join(','), map(({ machine, service: { port } }) => `${machine}:${port}`))(kafkaProcesses);
                        })(processes)
                    }
                ]
            )
        );
        let filename = commonTpl.replace('.tpl', '.properties');
        if (commonTpl.includes('share')) {
            filename = commonTpl.replace('.tpl', '.yml');
        }
        const dest = path.join(configDirPath, filename);
        let finalContent = renderer(commonTpl);
        for (let replaceTarget of replaceTargets) {
            finalContent = replaceTarget({ env, services, processes })(finalContent);
        }
        fs.writeFileSync(
            dest,
            finalContent
        );
    }
}

function handleAppProperties(services, appTplDir, configDirPath) {
    const serviceAccessList = buildServiceAccessListGetter(services);
    const appTpls = fs.readdirSync(appTplDir);
    for (let appTpl of appTpls) {
        const renderer = render(
            fs.readFileSync(path.join(appTplDir, appTpl)).toString("utf8"),
            [
                Const.SERVICE_NAME.CARD,
                Const.SERVICE_NAME.RBAC,
                Const.SERVICE_NAME.SCHEMA,
                Const.SERVICE_NAME.VIEW,
                Const.SERVICE_NAME.FILTER,
                Const.SERVICE_NAME.MAIN,
                Const.SERVICE_NAME.NOTI,
                Const.SERVICE_NAME.HISTORY,
                Const.SERVICE_NAME.MULTI,
                Const.SERVICE_NAME.SYNC,
                Const.SERVICE_NAME.NSTATS,
                Const.SERVICE_NAME.DEVOPS,
                Const.SERVICE_NAME.OSS,
                Const.SERVICE_NAME.API,
            ].map(serviceName => ({
                value: serviceAccessList(serviceName),
                placeHolder: `%${serviceName}Host%`
            }))
        );
        const dest = path.join(configDirPath, appTpl.replace('.tpl', '.properties'));
        fs.writeFileSync(
            dest,
            renderer(appTpl)
        );
    }
}
