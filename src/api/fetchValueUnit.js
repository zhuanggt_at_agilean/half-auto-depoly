const fetch = require('node-fetch');
const config = require('../config');
const logToFile = require('../tools/logToFile');

module.exports = async ({ cookies, vuId }) => {
    const url = `http://${config.access.host}/api/v1/value-units/${vuId}?with=link`;
    return await fetch(
        url,
        {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Cookie": cookies
            }
        }
    )
        .then(res => res.json())
        .then(body => {
            logToFile(JSON.stringify(body));
            return body.resultValue;
        })
}
