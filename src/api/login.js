const config = require('../config');
const fetch = require('node-fetch');

const login = async () => fetch(
    `https://${config.access.host}/login`,
    {
        headers: {
            "accept": "application/json",
            "code": "",
            "content-type": "application/json;charset=UTF-8",
            "flag": "json",
        },
        body: JSON.stringify({
            username: config.access.username,
            password: config.access.password
        }),
        method: "POST"
    }
)
    .then(
        res => {
            if (res.status !== 200) {
                throw new Error(res.statusText);
            }
            return res.headers.raw()['set-cookie'];
        }
    );

module.exports = login;
