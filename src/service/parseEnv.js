const config = require('../config');
const { Env } = require('../model');
const {
    textValue,
    enumName,
    linkOppIds
} = require('./support');

module.exports = vu => new Env({
    user: textValue(config.properties.env.user)(vu),
    nasPath: textValue(config.properties.env.nasPath)(vu),
    appDir: textValue(config.properties.env.appDir)(vu),
    appConfigDir: textValue(config.properties.env.appConfigDir)(vu),
    appolo: enumName(config.properties.env.appolo)(vu) === '是',
    machines: linkOppIds(config.properties.env.linkToMachines)(vu),
    processes: linkOppIds(config.properties.env.linkToProcesses)(vu),
});