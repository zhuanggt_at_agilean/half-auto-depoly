const {
    prop,
    compose,
    propEq,
    find,
    map,
    head,
    defaultTo
} = require('ramda');

const name = prop('name');
const id = prop('id');

const wrapFieldValue = propertyId => compose(
    defaultTo({}),
    find(propEq('id', propertyId)),
    defaultTo([]),
    prop('fields')
);

const numberValue = propertyId => compose(
    prop('numValue'),
    wrapFieldValue(propertyId)
);

const textValue = propertyId => compose(
    prop('strValue'),
    wrapFieldValue(propertyId)
);

const enumName = propertyId => compose(
    prop('name'),
    defaultTo({}),
    prop('value'),
    wrapFieldValue(propertyId)
);

const linkOppIds = propertyId => compose(
    map(prop('opponentId')),
    defaultTo([]),
    prop(propertyId),
    prop('linkValues')
);

const linkOppId = propertyId => compose(
    head,
    defaultTo([]),
    linkOppIds(propertyId)
);

module.exports = {
    id,
    name,
    textValue,
    enumName,
    linkOppId,
    linkOppIds,
    numberValue
};