module.exports = {
    parseEnv: require('./parseEnv'),
    parseProcess: require('./parseProcess'),
    parseMachine: require('./parseMachine'),
    parseService: require('./parseService'),
    parseNamespace: require('./parseNamespace'),
    parsePlugin: require('./parsePlugin')
};
