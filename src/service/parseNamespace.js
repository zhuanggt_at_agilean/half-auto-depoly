const config = require('../config');
const { Namespace } = require('../model');
const {
    id,
    name,
    textValue,
    enumName,
    numberValue,
    linkOppIds,
    linkOppId
} = require('./support');

module.exports = valueUnit => new Namespace({
    name: name(valueUnit),
    id: id(valueUnit)
});