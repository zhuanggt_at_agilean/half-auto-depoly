const config = require('../config');
const { Machine } = require('../model');
const {
    id,
    name,
    textValue,
    enumName,
    numberValue,
    linkOppIds,
    linkOppId
} = require('./support');

module.exports = valueUnit => new Machine({
    ip: name(valueUnit),
    id: id(valueUnit)
});