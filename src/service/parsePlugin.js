const config = require('../config');
const { Plugin } = require('../model');
const {
    id,
    name,
    textValue,
    enumName,
    numberValue,
    linkOppIds,
    linkOppId
} = require('./support');

module.exports = valueUnit => new Plugin({
    name: name(valueUnit),
    id: id(valueUnit),
    jarFileName: textValue(config.properties.service.jarFileName)(valueUnit),
});