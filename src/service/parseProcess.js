const config = require('../config');
const { Process } = require('../model');
const {
    name,
    textValue,
    enumName,
    numberValue,
    linkOppIds,
    linkOppId
} = require('./support');

module.exports = valueUnit => new Process({
    name: name(valueUnit),
    serviceIndex: numberValue(config.properties.process.serviceIndex)(valueUnit),
    serviceDuty: textValue(config.properties.process.serviceDuty)(valueUnit),
    baseInstall: textValue(config.properties.process.baseInstall)(valueUnit),
    baseData: textValue(config.properties.process.baseData)(valueUnit),
    machine: linkOppId(config.properties.process.toMachine)(valueUnit),
    memory: numberValue(config.properties.process.memory)(valueUnit),
    memoryUnit: enumName(config.properties.process.memoryUnit)(valueUnit),
    service: linkOppId(config.properties.process.toService)(valueUnit),
    namespaces: linkOppIds(config.properties.process.namespaces)(valueUnit),
    plugins: linkOppIds(config.properties.process.toPlugin)(valueUnit),
});