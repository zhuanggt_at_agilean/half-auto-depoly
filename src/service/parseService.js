const config = require('../config');
const { Service } = require('../model');
const {
    id,
    name,
    textValue,
    enumName,
    numberValue,
    linkOppIds,
} = require('./support');

module.exports = valueUnit => new Service({
    id: id(valueUnit),
    type: enumName(config.properties.service.type)(valueUnit),
    name: name(valueUnit),
    namespaces: linkOppIds(config.properties.service.namespaces)(valueUnit),
    jarFileName: textValue(config.properties.service.jarFileName)(valueUnit),
    port: textValue(config.properties.service.port)(valueUnit)
});
