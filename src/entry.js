const config = require('./config');
const login = require('./api/login');
const fetchValueUnit = require('./api/fetchValueUnit');
const { Node } = require('./model');

const { parseEnv, parseProcess, parseMachine, parseService, parseNamespace, parsePlugin } = require('./service');
const { construct, prop, map, uniq, compose, flatten, concat, indexBy, defaultTo, groupBy } = require('ramda');
const pipe = require('./pipe');

(async () => {
    try {
        const cookies = await login();
        const fetchVu = vuId => fetchValueUnit({ cookies, vuId });
        const envValueUnit = await fetchValueUnit({ cookies, vuId: config.access.entry });
        const env = parseEnv(envValueUnit);

        const machines = (await Promise.all(compose(map(fetchVu), prop('machines'))(env)))
            .map(parseMachine);
        const processes = (await Promise.all(compose(map(fetchVu), prop('processes'))(env)))
            .map(parseProcess);
        const services = (await Promise.all(compose(map(fetchVu), uniq, map(prop('service')))(processes)))
            .map(parseService);
        const namespaces = (await Promise.all(compose(map(fetchVu), uniq, flatten, map(prop('namespaces')))(concat(processes, services))))
            .map(parseNamespace);
        const plugins = (await Promise.all(compose(map(fetchVu), uniq, flatten, map(prop('plugins')))(processes)))
            .map(parsePlugin);
        console.log(JSON.stringify({ plugins }, null, 4));
        const indexById = indexBy(prop('id'));
        const serviceLookup = indexById(services);
        const namespacesLookup = indexById(namespaces);
        const machineLookup = indexById(machines);
        const pluginsLookup = indexById(plugins);
        for (let process of processes) {
            process.initMachine(compose(prop('ip'), defaultTo({}), prop(process.machine))(machineLookup));
            process.initService({ service: compose(defaultTo({}), prop(process.service))(serviceLookup) });
            process.initNamespace(namespacesLookup);
            process.initPlugins(pluginsLookup);
        }
        const serviceToProcess = groupBy(compose(prop('id'), prop('service')))(processes);
        for (let service of services) {
            service.initNamespace(namespacesLookup);
            service.initHosts(serviceToProcess);
        }
        console.log(JSON.stringify({
            env,
            machines,
            processes,
            namespaces,
            serviceToProcess,
            plugins
        }, null, 4));

        const nodes = compose(map(construct(Node)), Object.entries, groupBy(prop('machine')))(processes);
        pipe({
            env,
            config,
            nodes,
            services,
            processes
        });
    } catch (e) {
        console.log(e);
    }
})();