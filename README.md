## WHAT

### 建模

- env，环境，dev，test，prod 等
- service，服务，静态页面是服务，zk，kafka 之类的也是服务，java 程序也是服务
- namespace，服务的特征，比如使用 MySQL，比如使用 redis，比如使用 shared（此处后面应该细化） 
- process，一个进程会运行一个服务，并且有可能有特殊的 namespace，比如 card-hollow
- machine，机器，ip 为标识

### 生成脚本以及格式

比如：

- ssh config
    - 全部 process 都会有一个配置，映射到对应的机器上 (done)
- 普罗米修斯配置文件
    - 监控的 target 是 machine，但是为了标明所有的进程，会在 label 上标记清楚所有的 process (done)
- 批量调用脚本的脚本 (done)
- 服务启动脚本
- 服务检测脚本
- zk，kakfa 的配置文件，建 topic，查看 topic 脚本

## HOW

> 安装依赖
```bash
yarn
```

> 执行：本地新建 `.local.json`(这个名字会被排除出版本控制)，参考 `config-template.json` 文件
```bash
yarn dev
```

## TODO

### docker 化

### 自定义脚本

> 指定 `env.config.template` 的目录，编写 js 文件，会自动加载并执行，文件格式：

```javaScript
module.exports = ({ config, env, nodes, services, processes }) => {

}
```

// 恒丰实践的结果
- properties 文件没有全部在脚本中替换，这个后续增加
- view1 绑定 filter1，view2 绑定 filter2 这种咋整
- 复制 sh 脚本到远程机器的时候，要手动在目标目录 chmod +x，在 nas 上改了权限后权限复制不过去
- 有些远程脚本执行的时候，不会加载 PATH 到 shell 里，远程执行脚本要手动 source .bash_profile，或者再研究下，远程脚本执行的时候加载是哪一个 profile/rc 文件
- namespace 合并有问题
- 单个服务多个进程，内存参数不一致，都读取成一个了